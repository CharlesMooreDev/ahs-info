require('./bootstrap');

Vue.component('example', require('./app/Example.vue'));
Vue.component('main-content', require('./app/Content.vue'));

const app = new Vue({
    el: '#app'
});